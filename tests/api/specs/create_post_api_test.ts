import { checkStatusCode, checkResponseTime, checkJsonScheme } from "../../helpers/functionsForChecking.helper";
import { PostsController } from "../lib/controllers/posts.controller";
import { RegisterController } from "../lib/controllers/register.controllers";

const schema = require('./data/posts_scheme.json');
const chai = require('chai');
chai.use(require('chai-json-schema'))

const posts = new PostsController();
const registration = new RegisterController();

let postId: number;

let userId: number;
let accessToken: string;

describe('Create post tests', () => {
    before(async () => {
        let response = await posts.getAllPosts();
        postId = response.body[0].id;

        let registrationResponse = await registration.createNewUser(global.appConfig.users.testUser);
        
        userId = registrationResponse.body.user.id;
        accessToken = registrationResponse.body.token.accessToken.token;
    })

    it('should not allow to create post for unauthorised user', async () => {
        let response = await posts.createPost(1, 'image', 'post text', '');
        
        checkStatusCode(response, 401);
        checkResponseTime(response, 1000);
    })

    it('add like to post for unauthorised user', async () => {
        let response = await posts.likePost(postId, true, 1, '');
        
        checkStatusCode(response, 401);
        checkResponseTime(response, 1000);
    })

    it('create post for authorised user', async () => {
        let response = await posts.createPost(userId, global.appConfig.posts.testPost.image, global.appConfig.posts.testPost.text, accessToken);
        
        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkJsonScheme(response, schema.scheme_onePost);
    });

    it('add like to post for authorised user', async () => {
        let response = await posts.likePost(postId, true, userId, accessToken);
        
        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
    })

    it('remove like from post', async () => {
        let response = await posts.likePost(postId, false, userId, accessToken);
        
        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
    })
})