import { PostsController } from "../lib/controllers/posts.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";
import { checkJsonScheme } from "../../helpers/functionsForChecking.helper";
import { checkBodyLength } from "../../helpers/functionsForChecking.helper";

const posts = new PostsController();

const schema = require('./data/posts_scheme.json');
const chai = require('chai');
chai.use(require('chai-json-schema'))

describe('Posts controller', () => {
    it('should return all posts', async () => {
        let response = await posts.getAllPosts();

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkBodyLength(response, 1);
        checkJsonScheme(response, schema.scheme_allPosts);
    });
});