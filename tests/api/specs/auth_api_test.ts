import { checkJsonScheme, checkResponseTime, checkStatusCode } from "../../helpers/functionsForChecking.helper";
import { AuthController } from "../lib/controllers/auth.controller";
import { RegisterController } from "../lib/controllers/register.controllers";
import { UsersController } from "../lib/controllers/users.controller";

const schema = require('./data/users_scheme.json');
const chai = require('chai');
chai.use(require('chai-json-schema'))

const registration = new RegisterController();
const auth = new AuthController();
const users = new UsersController();

let logInUserId: number;
let accessToken: string;

let incorrectUsersData = [
    {email: 'testUser@gmail.com', password: ''},
    {email: 'testUser@gmail.com', password: '   '},
    {email: 'testUser@gmail.com', password: 'tgtgtgtg'},
    {email: 'testUser@gmail.com', password: '23A4Test202333! '},
    {email: 'testUser@gmail.com', password: ' 23A4Test202333!'}
]

describe('Auth Controller', () => {
    before(async () => {
        let response = await registration.createNewUser(global.appConfig.users.testUser);
        
        logInUserId = response.body.user.id;
        accessToken = response.body.token.accessToken.token;
    })

    incorrectUsersData.forEach((credentials) => {
        it(`should not login using invalid credentials : '${credentials.email}' + '${credentials.password}'`, async () => {
            let response = await auth.login(credentials.email, credentials.password);

            checkStatusCode(response, 401)  ; 


            checkResponseTime(response, 3000);
        });
    });

    it ('should log in existing user' , async () =>{
        let response = await auth.login(global.appConfig.users.testUser.email, global.appConfig.users.testUser.password)

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkJsonScheme(response, schema.scheme_authUser); 
    })

    it ('should return loginned user by access token' , async () => {
        let responseToken = await users.getUserByToken(accessToken);

        checkStatusCode(responseToken, 200);
        checkResponseTime(responseToken,1000);
        checkJsonScheme(responseToken, schema.scheme_oneUser);
    })

    after(async () => {
        await users.deleteUser(logInUserId, accessToken)
    })
})
