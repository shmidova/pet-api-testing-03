import { RegisterController } from "../lib/controllers/register.controllers";
import { checkJsonScheme, checkResponseTime, checkStatusCode } from "../../helpers/functionsForChecking.helper";
import { UsersController } from "../lib/controllers/users.controller";

const schema = require('./data/users_scheme.json');
const chai = require('chai');
chai.use(require('chai-json-schema'))

const registration = new RegisterController();
const users = new UsersController();

let userId: number;
let accessToken: string;

describe(`Delete user`,  () => {
    before(async () => {
        let response = await registration.createNewUser(global.appConfig.users.testUser);
        
        userId = response.body.user.id;
        accessToken = response.body.token.accessToken.token;
    })

    it('should delete user', async () => {
        console.log('should delete user')
        let response = await users.deleteUser(userId, accessToken);

        checkStatusCode(response, 200)
    })

    it('check that user was deleted', async () => {
        let response = await users.getUserById(userId);
        checkStatusCode(response, 404)
    })
});