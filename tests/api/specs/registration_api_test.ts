import { RegisterController } from "../lib/controllers/register.controllers";
import { checkJsonScheme, checkResponseTime, checkStatusCode } from "../../helpers/functionsForChecking.helper";
import { UsersController } from "../lib/controllers/users.controller";

const schema = require('./data/users_scheme.json');
const chai = require('chai');
chai.use(require('chai-json-schema'))

const registration = new RegisterController();
const users = new UsersController();

let userId: number;
let accessToken: string;

describe(`Register Controller`,  () => {
    it('should create a new user' , async () => {
        let response = await registration.createNewUser(global.appConfig.users.testUser);
        
        userId = response.body.user.id;
        accessToken = response.body.token.accessToken.token;

        checkStatusCode(response, 201);
        checkResponseTime(response, 1000);
        checkJsonScheme(response, schema.scheme_authUser);
    })

    after(async () => {
        await users.deleteUser(userId, accessToken)
    })
});