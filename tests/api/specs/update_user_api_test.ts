import {expect} from 'chai'
import { UsersController } from "../lib/controllers/users.controller";
import { RegisterController } from "../lib/controllers/register.controllers";
import { checkJsonScheme, checkResponseTime, checkStatusCode } from "../../helpers/functionsForChecking.helper";

const schema = require('./data/users_scheme.json');
const chai = require('chai');
chai.use(require('chai-json-schema'))

const registration = new RegisterController();
const users = new UsersController();

let userId: number;
let accessToken: string;
let userDataBeforeUpdate = global.appConfig.users.testUser;
let userDataToUpdate = {
    ...global.appConfig.users.testUser,
    'userName': global.appConfig.users.testUser.userName + Math.random().toString(36).substring(2, 5)
}

describe('Update user', () => {
    before(async () => {
        let response = await registration.createNewUser(global.appConfig.users.testUser);
        
        userId = response.body.user.id;
        accessToken = response.body.token.accessToken.token;
    })

    it ('should update User' , async () => {
        userDataToUpdate.id = userId;
        let responseUpdate = await users.updateUser(userDataToUpdate, accessToken)
    
        checkStatusCode(responseUpdate, 200);
        checkResponseTime(responseUpdate,1000);
    })
    
    it ('should return user by access token and check that username was updated' , async () => {
        let response = await users.getUserByToken(accessToken);
        
        checkStatusCode(response, 200);
        checkJsonScheme(response, schema.scheme_oneUser);
        
        expect(response.body.userName, `UserName changed from ${userDataBeforeUpdate.userName} to ${userDataToUpdate.userName}`).to.be.equal(userDataToUpdate.userName);
    })
    
    it('should return user details when getting user with valid id' , async () => {
        let response = await users.getUserById(userId);
        
        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
        checkJsonScheme(response, schema.scheme_oneUser);
    
    });
    
    after(async () => {
        await users.deleteUser(userId, accessToken)
    })
})
