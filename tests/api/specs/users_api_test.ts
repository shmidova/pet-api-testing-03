import { UsersController } from "../lib/controllers/users.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";
import { checkJsonScheme } from "../../helpers/functionsForChecking.helper";
import { checkBodyLength } from "../../helpers/functionsForChecking.helper";

const schema = require('./data/users_scheme.json');
const chai = require('chai');
chai.use(require('chai-json-schema'))

const users = new UsersController();

describe('Users controller' , () => {
    it(`should return 404 error when getting user details with invalid id`, async () => {
        let invalidUserId = 123133

        let response = await users.getUserById(invalidUserId);
 
        checkStatusCode(response, 404);
        checkResponseTime(response,1000);
    });

    it(`should return 400 error when getting user details with invalid id type`, async () => {
        let invalidUserId = '2183821367281387213781263'

        let response = await users.getUserById(invalidUserId);

        checkStatusCode(response, 400);
        checkResponseTime(response, 1000);
    });

    it ('should return all users when getting the user collection' , async () => {
        let response = await users.getAllUsers();

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkJsonScheme(response, schema.scheme_allUsers);
        checkBodyLength(response, 1);
    });

    it(`should return user details when getting user details with valid id`, async () => {
        let response = await users.getAllUsers();
        let firstUserId: number = response.body[0].id;
        
        response = await users.getUserById(firstUserId);
        
        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
    });

});

