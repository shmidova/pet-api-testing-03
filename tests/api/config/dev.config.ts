global.appConfig = {
    envName: "DEV Environment",
    baseUrl: "http://tasque.lol/",
    swaggerUrl: "http://tasque.lol/index.html",

    users: {
        testUser: {
            email: "testUserEmail@gmail.com",
            password: "23A4Test202333!",
            userName: "myUsername",
            avatar: 'myAavatar'
        },
    },
    posts: {
        testPost: {
            image: 'postImage',
            text: 'random post text'
        }
    }
};
