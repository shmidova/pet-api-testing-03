import { ApiRequest } from "../request";

const baseUrl:string = "http://tasque.lol/";

export class RegisterController {
    async createNewUser(userData: object) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Register`)
            .body(userData)
            .send();

        return response;
    }
}