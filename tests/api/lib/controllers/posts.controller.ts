import { ApiRequest } from "../request";

const baseUrl:string = "http://tasque.lol/";

export class PostsController {
    async getAllPosts() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Posts`)
            .send();
        
        return response;
    }

    async createPost(authorId: number, preview: string, body: string, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts`)
            .body({
                authorId: authorId,
                previewImage: preview,
                body: body
            })
            .bearerToken(accessToken)
            .send();
        
        return response;
    }

    async likePost(postId: number, isLike: boolean, userId: number, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts/like`)
            .body({
                entityId: postId,
                isLike: isLike,
                userId: userId
            })
            .bearerToken(accessToken)
            .send();
        
        return response;
    }
}