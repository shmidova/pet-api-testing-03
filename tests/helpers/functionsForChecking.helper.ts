import { expect } from "chai";

export function checkStatusCode(response, statusCode: 200 | 201 | 204 | 400 | 401 | 403 | 404 | 409 | 500) {
    expect(response.statusCode, `Status Code should be ${statusCode}`).to.equal(statusCode);
}

export function checkResponseTime(response, maxResponseTime: number = 3000) {
    expect(response.timings.phases.total, `Response time should be less than ${maxResponseTime}ms`).to.be.lessThan(
        maxResponseTime
    );

}

export function checkJsonScheme (response, jsonSchema ) {
    expect(response.body).to.be.jsonSchema(jsonSchema)
}

export function checkBodyLength (response, bodyLength ) {
    expect(response.body.length, `Response body should have more than ${bodyLength} item`).to.be.greaterThan(bodyLength);

}